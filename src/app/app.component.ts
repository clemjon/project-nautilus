import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LibraryPage } from '../pages/library/library';
import { FormulaListPage } from '../pages/formulaList/formulaList';
//import { LoginPage } from '../pages/login/login';
import { SavedObjectsPage } from '../pages/savedObjects/savedObjects';

import { Splash } from '../pages/splash/splash';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LibraryPage;

  pages: Array<{title: string, component: any, icon: string}>;

  showSubmenu: boolean = false;

  menuItemHandler(): void {
    this.showSubmenu = !this.showSubmenu;
  }

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, modalCtrl: ModalController) {
    //this.initializeApp();

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      //this.splashScreen.hide();
      let splash = modalCtrl.create(Splash);
      splash.present();
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Manual', component: LibraryPage, icon: "document"},
      { title: 'Calculator', component: FormulaListPage, icon: "calculator"},
      { title: 'Saved Items', component: SavedObjectsPage, icon: "archive"}
      //{ title: 'Login', component: LoginPage, icon: "man"}
    ];    

  }

  initializeApp() {
    // this.platform.ready().then(() => {
    //   // Okay, so the platform is ready and our plugins are available.
    //   // Here you can do any higher level native things you might need.
    //   this.statusBar.styleDefault();
    //   //this.splashScreen.hide();
    //   let splash = this.modalCtrl.create(Splash);
    //   splash.present();
    // });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  openChapter(chapter) {

    
  }

  

}