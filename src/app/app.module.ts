import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SavedItemsPage } from '../pages/savedItems/savedItems';
import { CalculatorPage } from '../pages/calculator/calculator';
import { SavedCalculationsPage } from '../pages/savedCalculations/savedCalculations';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicStorageModule } from '@ionic/storage';
import { SQLite } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { LibraryPage } from '../pages/library/library';
import { FormulaListPage } from '../pages/formulaList/formulaList';
import { Formula1Page } from '../pages/formula1/formula1';
import { Formula2Page } from '../pages/formula2/formula2';
import { Formula3Page } from '../pages/formula3/formula3';
import { Formula4Page } from '../pages/formula4/formula4';
import { Formula5Page } from '../pages/formula5/formula5';
import { Formula6Page } from '../pages/formula6/formula6';
import { Formula7Page } from '../pages/formula7/formula7';
import { Formula8Page } from '../pages/formula8/formula8';
import { Formula9Page } from '../pages/formula9/formula9';
import { Formula10Page } from '../pages/formula10/formula10';
import { Formula11Page } from '../pages/formula11/formula11';
import { Formula12Page } from '../pages/formula12/formula12';
import { Formula13Page } from '../pages/formula13/formula13';
import { DocumentViewer } from '@ionic-native/document-viewer';

import { Chapter1 } from '../pages/chapter1/chapter1';
import { Chapter2 } from '../pages/chapter2/chapter2';
import { Chapter3 } from '../pages/chapter3/chapter3';
import { Chapter4 } from '../pages/chapter4/chapter4';
import { Chapter5 } from '../pages/chapter5/chapter5';
import { Chapter6 } from '../pages/chapter6/chapter6';
import { Chapter7 } from '../pages/chapter7/chapter7';
import { Chapter8 } from '../pages/chapter8/chapter8';
import { Chapter9 } from '../pages/chapter9/chapter9';
import { Chapter10 } from '../pages/chapter10/chapter10';
import { Chapter11 } from '../pages/chapter11/chapter11';
import { Chapter12 } from '../pages/chapter12/chapter12';
import { Chapter13 } from '../pages/chapter13/chapter13';
import { Chapter14 } from '../pages/chapter14/chapter14';
import { Chapter15 } from '../pages/chapter15/chapter15';
import { Chapter16 } from '../pages/chapter16/chapter16';
import { Chapter17 } from '../pages/chapter17/chapter17';
import { Chapter18 } from '../pages/chapter18/chapter18';
import { Chapter19 } from '../pages/chapter19/chapter19';

import { Dialogs } from '@ionic-native/dialogs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import { SavedObjectsPage } from '../pages/savedObjects/savedObjects';
import { Splash } from '../pages/splash/splash';
import { HttpClientModule } from '@angular/common/http';
import { ConversionFormulaPage } from '../pages/conversionFormula/conversionFormula';
import { Formula16Page } from '../pages/formula16/formula16';
import { Formula17Page } from '../pages/formula17/formula17';
import { Formula18Page } from '../pages/formula18/formula18';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    Splash,
    RegisterPage,
    SavedItemsPage,
    CalculatorPage,
    SavedCalculationsPage,
    SavedObjectsPage,
    LibraryPage,
    FormulaListPage,
    Formula1Page,
    Formula2Page,
    Formula3Page,
    Formula4Page,
    Formula5Page,
    Formula6Page,
    Formula7Page,
    Formula8Page,
    Formula9Page,
    Formula10Page,
    Formula11Page,
    Formula12Page,
    Formula13Page,
    Formula16Page,
    Formula17Page,
    Formula18Page,
    Chapter1,
    Chapter2,
    Chapter3,
    Chapter4,
    Chapter5,
    Chapter6,
    Chapter7,
    Chapter8,
    Chapter9,
    Chapter10,
    Chapter11,
    Chapter12,
    Chapter13,
    Chapter14,
    Chapter15,
    Chapter16,
    Chapter17,
    Chapter18,
    Chapter19,
    ConversionFormulaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    Splash,
    RegisterPage,
    SavedItemsPage,
    CalculatorPage,
    SavedCalculationsPage,
    SavedObjectsPage,
    LibraryPage,
    FormulaListPage,
    Formula1Page,
    Formula2Page,
    Formula3Page,
    Formula4Page,
    Formula5Page,
    Formula6Page,
    Formula7Page,
    Formula8Page,
    Formula9Page,
    Formula10Page,
    Formula11Page,
    Formula12Page,
    Formula13Page,
    Formula16Page,
    Formula17Page,
    Formula18Page,
    Chapter1,
    Chapter2,
    Chapter3,
    Chapter4,
    Chapter5,
    Chapter6,
    Chapter7,
    Chapter8,
    Chapter9,
    Chapter10,
    Chapter11,
    Chapter12,
    Chapter13,
    Chapter14,
    Chapter15,
    Chapter16,
    Chapter17,
    Chapter18,
    Chapter19,
    ConversionFormulaPage
    
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLite,
    Toast,
    DocumentViewer,
    Dialogs
  ]
})
export class AppModule {}
