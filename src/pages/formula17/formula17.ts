import { Component, ViewChild } from '@angular/core';
import { NavController, Content, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { Dialogs } from '@ionic-native/dialogs';

@Component({
  selector: 'page-formula17',
  templateUrl: 'formula17.html'
})
export class Formula17Page {

  t1: any;
  t2: any;
  v1: any;

  p1: any;
  t3: any;
  t4: any;


  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController, private sqlite: SQLite, 
      private toast: Toast,
      public navParams: NavParams,
      private dialogs: Dialogs) {

    if( navParams.get("var1") != null ){
      this.t1 = navParams.get("var1");
    }
  
    if( navParams.get("var2") != null ){
      this.t2 = navParams.get("var2");
    }
  
    if( navParams.get("var3") != null ){
      this.v1 = navParams.get("var3");
    }

  }

  ionViewDidEnter() {
    this.compute();
  }


  compute(){

    let temp1 = Number(this.t1) + 460;
    let temp2 = Number(this.t2) + 460;
    let a = (Number(this.v1) * Number(temp2)) / temp1;
    document.getElementById("result-v2").innerHTML = a.toFixed(2).toString();

  }

  computePressure(){
    let a = (Number(this.p1) + 14.7) * (Number(this.t4) + 460) / (Number(this.t3) + 460);
    a = Number(a) - 14.7;
    document.getElementById("result-pressure").innerHTML = a.toFixed(2).toString();
  }

}
