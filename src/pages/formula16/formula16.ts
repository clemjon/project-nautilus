import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';

@Component({
  templateUrl: 'formula16.html'
})

export class Formula16Page {

  pressure: any;
  volume: any;
  n: any;

  d1: any;
  volume1: any;
  d2: any;

  d3: any;
  volume2: any;
  volume3: any;


  @ViewChild(Content) content: Content;
  constructor() {

  }

  ionViewDidEnter() {
  }

  computeFinalVolume(){
    let ata1 = (Number(this.d1) + 33) / 33;
    let ata2 = (Number(this.d2) + 33) / 33;
    let a = ata1*this.volume1/ata2;
    document.getElementById("result-finalVolume").innerHTML = a.toFixed(2).toString();
  }

  computeFinalDepth(){
    let ata1 = (Number(this.d3) + 33) / 33;
    let a = ata1*this.volume2/this.volume3;
    document.getElementById("result-finalDepth").innerHTML = a.toFixed(2).toString();
  }

}
