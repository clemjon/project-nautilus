import { Component, ViewChild } from '@angular/core';
import { NavController, Content, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-formula8',
  templateUrl: 'formula8.html'
})
export class Formula8Page {

  pressure: any;
  fv: any;

  scf: any;
  fv2: any;

  scf2: any;
  pressure2: any;

  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    if( navParams.get("var1") != null ){
      this.scf = navParams.get("var1");
    }
  
    if( navParams.get("var2") != null ){
      this.pressure = navParams.get("var2");
    }
  
    if( navParams.get("var3") != null ){
      this.fv = navParams.get("var3");
    }
  }

  ionViewDidEnter() {
    // this.compute();
  }


  computeScf(){
    let a = (( Number(this.pressure) + 14.7 ) / 14.7) * this.fv;
    document.getElementById("result-scf").innerHTML = a.toFixed(2).toString();
  }
  
  computePressure(){
    let b = (Number(this.scf) / Number(this.fv2)).toFixed(5);
    let c = ((Number(b) - 1) * 14.7).toFixed(2).toString();
    this.pressure = Number(c);
    document.getElementById("result-pressure").innerHTML = (Math.round(Number(c) + 0.5)).toString();
  }
}
