import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
// import { Observable } from 'rxjs/Observable';
// import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  username: any;
  firstname: any;
  lastname: any;
  email: any;
  password: any;
  confirmPassword: any;
  // test: Observable<any>;

  constructor(public navCtrl: NavController, private sqlite: SQLite, 
    private toast: Toast
    // , public httpClient: HttpClient
  ) {
      // this.test = this.httpClient.get('http://stilustrata.com/usnapi/test.php');
      // console.log(this.test)
      // this.test
      // .subscribe(data => {
      //   console.log('my data: ', data.response);
      // })
  }

  login() {}

  register() {
    if( this.confirmPassword == undefined || this.confirmPassword == undefined || this.confirmPassword == "" || this.confirmPassword == "" || this.confirmPassword != this.password){
      alert("Password does not match the confirm password.");
    } else if ( this.username == "" || this.firstname == "" || this.lastname == "" || this.email == "" || 
                this.username == undefined || this.firstname == undefined || this.lastname == undefined || this.email == undefined ) {
      alert("All fields are required.");
    } else {
      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql("INSERT INTO users (date_created, username, first_name, last_name, email, password) VALUES(strftime('%Y-%m-%d %H-%M-%S','now'),?,?,?,?,?)",[this.username, this.firstname, this.lastname, this.email, this.password])
          .then(res => {
            console.log(res);
            this.toast.show('Successfully registered.', '1000', 'center').subscribe(
              toast => {}
            );
          })
          .catch(e => {
            console.log(e);
            this.toast.show(e, '1000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
          });
      }).catch(e => {
        console.log(e);
        this.toast.show(e, '1000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      });
    }
    
  }

}
