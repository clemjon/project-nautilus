import { Component, ViewChild } from '@angular/core';
import { NavController, Content, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { Dialogs } from '@ionic-native/dialogs';

@Component({
  selector: 'page-chapter4',
  templateUrl: 'chapter4.html'
})
export class Chapter4 {

  @ViewChild(Content) content: Content;
  data = { date:"", type:"", description:"", amount:0 };
  selectedText = "";
  public selectedChapter : any;
  subchapter = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private sqlite: SQLite,
    private toast: Toast,
    private dialogs: Dialogs) {

      this.selectedChapter = navParams.get("chapterParam");
      this.selectedText = navParams.get("description");
      this.subchapter = navParams.get("subchapter");

  }

  save(){
    this.selectedText = window.getSelection().toString();
    
    if ( this.selectedText != "" ) {
      this.selectedText = window.getSelection().getRangeAt(0).toString();
    }
    if ( this.selectedText != "" ) {
      this.selectedText = document.getSelection().getRangeAt(0).toString();
    }
    if ( this.selectedText != "" ) {
      this.selectedText = document.getSelection().toString();
    }

    if( this.selectedText != "" ){

    this.dialogs.prompt("Add Note", "", ["Save", "Cancel"], "")
    .then((result) => 
    { 
      if(result.buttonIndex == 1) {
        this.saveData(result.input1);
      }
    })
    .catch(e => alert("Error in saving calculation"));

    } else {
      alert("Reselect text");
    }
  }

  saveData(note) {
    
    

      this.sqlite.create({
        name: 'ionicdb.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql("INSERT INTO saved_text VALUES(NULL,strftime('%Y-%m-%d %H-%M-%S','now'),?,?,?)",[this.selectedText, note, '4'])
          .then(res => {
            console.log(res);
            this.toast.show('Data saved', '1000', 'center').subscribe(
              toast => {
                this.navCtrl.popToRoot();
              }
            );
          })
          .catch(e => {
            console.log(e);
            this.toast.show(e, '1000', 'center').subscribe(
              toast => {
                console.log(toast);
              }
            );
          });
      }).catch(e => {
        console.log(e);
        this.toast.show(e, '1000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      });
    
  }

  ionViewDidEnter() {
    if( this.selectedText != undefined && this.selectedText != "" ){
      this.content.getNativeElement().innerHTML = this.content.getNativeElement().innerHTML.replace(this.selectedText, "<span id='highlightedText' class='highlightedText'>"+this.selectedText+"</span>");
      this.highlight();
    }

    if(this.subchapter!="" && this.subchapter!=undefined){
      console.log(this.subchapter)
      let yOffset = document.getElementById(this.subchapter);
      yOffset.scrollIntoView()
    }
  }

  highlight()
  {
    let element = document.getElementById('highlightedText');
    element.scrollIntoView()
  }

}
