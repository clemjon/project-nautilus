import { Component, ViewChild } from '@angular/core';
import { NavController, Content, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-formula18',
  templateUrl: 'formula18.html'
})
export class Formula18Page {

  depth: any;
  oxygen: any;

  gas: any;
  oxygen2: any;

  depth2: any;
  gas2: any;

  myItemsList = [0];

  addItem() {
    this.myItemsList.push(0);
  }

  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController,
      public navParams: NavParams) {

    // if( navParams.get("var1") != null ){
    //   this.t1 = navParams.get("var1");
    // }
  
    // if( navParams.get("var2") != null ){
    //   this.t2 = navParams.get("var2");
    // }
  
    // if( navParams.get("var3") != null ){
    //   this.v1 = navParams.get("var3");
    // }

  }

  ionViewDidEnter() {
  }


  compute(){

    let pressureTotal = 0;
    for(let i=0; i<this.myItemsList.length; i++ ){
      pressureTotal += Number(this.myItemsList[i]);
    }
    document.getElementById("result-pressureTotal").innerHTML = pressureTotal.toFixed(2).toString();

  }

  computePercent(){
    let ata = (Number(this.depth) + 33) / 33;
    let a = (Number(this.oxygen) / ata) * 100;
    document.getElementById("result-percent").innerHTML = a.toFixed(2).toString();
  }

  computeDepth(){
    let percentage = Number(this.gas) / 100;
    let a = Number(this.oxygen2) / percentage;
    let ata = (Number( Number(a.toFixed(2)) - 0.005) * 33) - 33;
    document.getElementById("result-depth").innerHTML = Math.round(Number(ata) - 0.5).toString();
  }

  computeOxy(){
    let ata = ((Number(this.depth2) + 33) / 33) - 0.005;
    let a = ((Number(this.gas2) / 100) *  Number(ata.toFixed(2))) - 0.005;
    document.getElementById("result-oxy").innerHTML = a.toFixed(2).toString();
  }
}
