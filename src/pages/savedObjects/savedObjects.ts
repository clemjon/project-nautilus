import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SavedItemsPage } from '../savedItems/savedItems';
import { SavedCalculationsPage } from '../savedCalculations/savedCalculations';

@Component({
  selector: 'page-savedObjects',
  templateUrl: 'savedObjects.html'
})
export class SavedObjectsPage {

  pages: Array<{title: string, component: any, icon: string}>;

  constructor(public navCtrl: NavController) {
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Bookmarks', component: SavedItemsPage, icon: "bookmark"},
      { title: 'Calculations', component: SavedCalculationsPage, icon: "flask"}
    ];    

  }

  openChapter(component) {
    this.navCtrl.push(component, {});
  }
}
