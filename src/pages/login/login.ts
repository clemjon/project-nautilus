import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  username: any;
  password: any;

  constructor(public navCtrl: NavController, private sqlite: SQLite,public platform: Platform,
    public splashscreen: SplashScreen) {
      platform.ready().then(() => {
        this.splashscreen.hide();
      });
  }

  login() {
    this.sqlite.create({
      name: 'ionicdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM USERS WHERE username=? and password=?', [this.username, this.password])
      .then(res => {
        
        if( res.rows.length > 0 ){
          alert("login successful");
        } else {
          alert("User not found");
        }
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  register() {
    this.navCtrl.push(RegisterPage, {});
  }

  continueOffline(){
    this.navCtrl.setRoot(HomePage, {});
  }

}
