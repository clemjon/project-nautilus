import { Component, ViewChild } from '@angular/core';
import { NavController, Content, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-conversionFormula',
  templateUrl: 'conversionFormula.html'
})
export class ConversionFormulaPage {

  ata: any;
  depth: any;
  psig: any;
  ata2: any;
  depth2: any;
  psig2: any;
  psig3: any;
  percent: any;
  decimal: any;
  decimal2: any;
  minutes: any;
  ata3: any;
  mmhg: any;
  percent2: any;
  ppm: any;
  airTime: any;
  o2Time: any;
  o2TimeRemaining: any;
  fahrenheit: any;
  celsius: any;
  fahrenheit2: any;
  celsius2: any;
  co2: any;
  fsw: any;
  fsw2: any;
  fsw3: any;
  acf: any;
  scf: any;
  fsw4: any;
  fsw5: any;
  gas: any;

  @ViewChild(Content) content: Content;
  constructor(public navCtrl: NavController,public navParams: NavParams) {}

  ionViewDidEnter() {
  }


  computeAta(){
    let a = (Number(this.depth) + 33) / 33;
    document.getElementById("result-ata").innerHTML = a.toFixed(2) + " ATA";
  }

  computeDepth(){
    let a = (Number(this.ata) - 1) * 33;
    document.getElementById("result-depth").innerHTML = Math.round(a).toFixed(2) + " fsw";
  }

  psigToAta2(){
    let a = (Number(this.psig) + 14.7) / 14.7;
    document.getElementById("result-ata2").innerHTML = a.toFixed(2) + " ATA";
  }

  ata2ToPsig(){
    let a = (Number(this.ata2) - 1) * 14.7;
    document.getElementById("result-psig").innerHTML = Math.round(a).toFixed(2) + " PSIG";
  }

  fswToPsig(){
    let a = Number(this.depth2) * 0.445;
    document.getElementById("result-psig2").innerHTML = Math.round(Number(a) + 0.5).toFixed(2) + " PSIG";
  }

  psigToFsw(){
    let a = Number(this.psig2) / 0.445;
    document.getElementById("result-depth2").innerHTML = Math.round(Number(a) + 0.5).toFixed(2) + " fsw";
  }

  ppOfGas(){
    let ata = (Number(this.fsw5) + 33) / 33; 
    let a = (Number(ata.toFixed(2))) * Number(this.gas/100);
    document.getElementById("result-ppOfGas").innerHTML = Number(a).toFixed(2) + " PP in ATA";
  }
 
  psigToPsia(){
    let a = Number(this.psig3) + 14.7;
    document.getElementById("result-psia").innerHTML = Math.round(Number(a) + 0.5).toFixed(2) + " PSIA";
  }

  percentToDec(){
    let a = Number(this.percent) / 100;
    document.getElementById("result-decimal").innerHTML = Number(a).toFixed(2);
  }

  decToPercent(){
    let a = Number(this.decimal) * 100;
    document.getElementById("result-percent").innerHTML = a + "%";
  }

  decToMinutes(){
    let a = Number(this.decimal2) * 60;
    document.getElementById("result-minutes").innerHTML = Number(a).toFixed(2)+ " minutes of seconds";
  }

  minutesToDec(){
    let a = Number(this.minutes) / 60;
    document.getElementById("result-decimal2").innerHTML = Number(a).toFixed(2)+ " decimal";
  }

  ataToMmhg(){
    let a = Number(this.ata3) * 760;
    document.getElementById("result-mmhg").innerHTML = Number(a).toFixed(2)+ " mmHg";
  }

  mmhgToAta(){
    let a = Number(this.mmhg) / 760;
    document.getElementById("result-ata4").innerHTML = Number(a).toFixed(2)+ " ATA";
  }

  percentToPpm(){
    let a = Number(this.percent2) * 10000;
    document.getElementById("result-ppm").innerHTML = Number(a).toFixed(2)+ " PPM";
  }

  ppmToPercent(){
    let a = Number(this.ppm) / 10000;
    document.getElementById("result-percent3").innerHTML = Number(a).toFixed(2)+ "%";
  }

  computeRatio(){
    let a = Number(this.airTime) / Number(this.o2Time);
    document.getElementById("result-ratio").innerHTML = Number(a).toFixed(2)+ " Air/O2 Trading Ratio";
  }

  computeO2Period(){
    let a = Number(this.o2TimeRemaining) / 10000;
    document.getElementById("result-o2period").innerHTML = Number(a).toFixed(2)+ "%";
  }

  fahrenheitToCelsius(){
    let a = ((Number(this.fahrenheit) - 32) * 5)/9;
    document.getElementById("result-celsius").innerHTML = Number(a).toFixed(2)+ " Celcius";
  }

  celsiusToFahrenheit(){
    let a = (Number(this.celsius) * 1.8) + 32;
    document.getElementById("result-fahrenheit").innerHTML = Number(a).toFixed(2)+ " Farenheit";
  }

  fahrenheitToAbsolute(){
    let a = Number(this.fahrenheit2) + 460;
    document.getElementById("result-absolute").innerHTML = Number(a).toFixed(2)+ " Degress Rankine";
  }

  celsiusToAbsolute(){
    let a = Number(this.celsius2) + 273;
    document.getElementById("result-absolute2").innerHTML = Number(a).toFixed(2)+ " Degress Kelvin";
  }

  sev(){
    let ata = (Number(this.fsw) + 33) / 33; 
    let a = Number(ata.toFixed(2)) * (this.co2);
    document.getElementById("result-sev").innerHTML = a.toFixed(2) + " SEV";
  }
  
  scfToAcf(){
    let ata = (Number(this.fsw2) + 33) / 33; 
    let a = (this.scf) / Number(ata.toFixed(2));
    document.getElementById("result-acf").innerHTML = a.toFixed(2) + " acf";
  }

  acfToScf(){
    let ata = (Number(this.fsw3) + 33) / 33; 
    let a = (this.acf) * Number(ata.toFixed(2));
    document.getElementById("result-scf").innerHTML = a.toFixed(2) + " scf";
  }

  co2Sev(){
    let ata = (Number(this.fsw4) + 33) / 33; 
    let a = (1.5 / Number(ata.toFixed(2))) * 100;
    document.getElementById("result-co2Sev").innerHTML = a.toFixed(2) + " CO2%";
  }
}
