import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Chapter1 } from '../chapter1/chapter1';
import { Chapter2 } from '../chapter2/chapter2';
import { Chapter3 } from '../chapter3/chapter3';
import { Chapter4 } from '../chapter4/chapter4';
import { Chapter5 } from '../chapter5/chapter5';
import { Chapter6 } from '../chapter6/chapter6';
import { Chapter7 } from '../chapter7/chapter7';
import { Chapter8 } from '../chapter8/chapter8';
import { Chapter9 } from '../chapter9/chapter9';
import { Chapter10 } from '../chapter10/chapter10';
import { Chapter11 } from '../chapter11/chapter11';
import { Chapter12 } from '../chapter12/chapter12';
import { Chapter13 } from '../chapter13/chapter13';
import { Chapter14 } from '../chapter14/chapter14';
import { Chapter15 } from '../chapter15/chapter15';
import { Chapter16 } from '../chapter16/chapter16';
import { Chapter17 } from '../chapter17/chapter17';
import { Chapter18 } from '../chapter18/chapter18';
import { Chapter19 } from '../chapter19/chapter19';

@Component({
  selector: 'page-list',
  templateUrl: 'savedItems.html'
})
export class SavedItemsPage {

  savedTexts: any = [];

  constructor(public navCtrl: NavController, private sqlite: SQLite) {}

  ionViewDidLoad() {
    this.getData();
  }
  
  ionViewWillEnter() {
    this.getData();
  }
  
  getData() {
    this.sqlite.create({
      name: 'ionicdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('SELECT * FROM saved_text ORDER BY rowid DESC', {})
      .then(res => {
        this.savedTexts = [];
        for(var i=0; i<res.rows.length; i++) {
          this.savedTexts.push(
            {
              rowid:res.rows.item(i).rowid,
              date:res.rows.item(i).date,
              description:res.rows.item(i).description,
              note:res.rows.item(i).note,
              chapter:res.rows.item(i).chapter
            }
          )
        }
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  deleteData(rowid) {
    this.sqlite.create({
      name: 'ionicdb.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      db.executeSql('DELETE FROM saved_text WHERE rowid=?', [rowid])
      .then(res => {
        console.log(res);
        this.getData();
      })
      .catch(e => console.log(e));
    }).catch(e => console.log(e));
  }

  goToManual(description, chapter) {
    
    let page : any;

    if (chapter == 1) {
      page = Chapter1;
    } else if (chapter == 2) {
      page = Chapter2;
    } else if (chapter == 3) {
      page = Chapter3;
    } else if (chapter == 4) {
      page = Chapter4;
    } else if (chapter == 5) {
      page = Chapter5;
    } else if (chapter == 6) {
      page = Chapter6;
    } else if (chapter == 7) {
      page = Chapter7;
    } else if (chapter == 8) {
      page = Chapter8;
    } else if (chapter == 9) {
      page = Chapter9;
    } else if (chapter == 10) {
      page = Chapter10;
    } else if (chapter == 11) {
      page = Chapter11;
    } else if (chapter == 12) {
      page = Chapter12;
    } else if (chapter == 13) {
      page = Chapter13;
    } else if (chapter == 14) {
      page = Chapter14;
    } else if (chapter == 15) {
      page = Chapter15;
    } else if (chapter == 16) {
      page = Chapter16;
    } else if (chapter == 17) {
      page = Chapter17;
    } else if (chapter == 18) {
      page = Chapter18;
    } else if (chapter == 19) {
      page = Chapter19;
    }

    this.navCtrl.push(page, {
      description: description
    });
  }

}
